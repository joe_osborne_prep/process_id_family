#!/bin/bash

source pidFunc.sh

if [[ $# -ne 1 ]]
then
	echo Please provide a process ID
else

	currentCPID=$1

	START=1
	END=3
	pidList=$(tripleAncestor $currentCPID $START $END)


	children=$(getChild $currentCPID) 
	childrenList=$children
	
	for child in $childrenList 
	do 	
		grandChildren=$(getChild $child)
		for gChild in $grandChildren
		do
			ggChild=$(getChild $gChild)	
			ggChildList=$ggChild,$ggChildList
		done
		gChildList=$grandChildren,$gChildList
	done
	
	for i in $childrenList
	do
		childList2=$childList2,$i
	done

	for i in $gChildList
	do
		gChildList2=$gCchildList2,$i

	done
	for i in $ggChildList
	do
		ggChildList2=$ggChildList2,$i
	done

################################################################################################################
	
	lists=("$currentCPID" "$pidList" "$childList2" "$gChildList2" "$ggChildList2" )

IFS=','
	for (( a=0; a<${#lists[@]};a++ ))
	do

		case $a in
			0) listName="Starting Process";;
			1) listName="Ancestor";;
			2) listName="Child";;
			3) listName="GrandChild";;
			4) listName="Great GrandChild";;
			*) listName="Unknown list";;
		esac
		
		curList=${lists[$a]}
		echo
		#echo list is $listName :  $curList
		echo 

		for i in ${curList[@]}	
		do
			if [[ -z $i ]] || [[ $i -le 0  ]]
			then
				:		
			else
				pidName=`ps -p $i -o comm | awk 'FNR==2{print $0}'` 			
				if [[ -z $pidName  ]]
				then
					:
				else
	 				echo $listName :  PID : $i : $pidName	
					echo Network Data for : $pidName
					echo `sudo netstat -nlpa | grep $pidName | grep $i/|grep ESTABLISHED`
					echo
					echo
				fi
			fi
		done
	done
fi
